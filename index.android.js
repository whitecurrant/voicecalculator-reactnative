import { AppRegistry } from 'react-native';
import App from './components/App';

AppRegistry.registerComponent('VoiceCalculator', () => App);
