package com.voicecalculator.speech;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Marcin Adamczyk <mcn.adamczyk@gmail.com> on 4/09/17.
 */

class SpeechRecognitionModule extends ReactContextBaseJavaModule {

    private final static String ON_SPEECH_STARTED = "ON_SPEECH_STARTED";
    private final static String ON_SPEECH_STOPPED = "ON_SPEECH_STOPPED";
    private final static String ON_PARTIAL_RESULTS = "ON_PARTIAL_RESULTS";
    private final static String ON_RESULTS = "ON_RESULTS";
    private final static String ON_ERROR = "ON_ERROR";
    private final static String TIMEOUT = "TIMEOUT";

    private SpeechRecognizer speechRecognizer;
    private ReactApplicationContext reactContext;
    private Handler mainThreadHandler;

    SpeechRecognitionModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
        mainThreadHandler = new Handler(reactContext.getMainLooper());
        mainThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                initializeSpeechRecognizer();
            }
        });
    }

    private void initializeSpeechRecognizer(){
        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(reactContext);
        speechRecognizer.setRecognitionListener(new RecognitionListener() {

            private String getFirstResult(Bundle bundle){
                ArrayList<String> results = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                return results == null || results.isEmpty() ? null: results.get(0);
            }

            @Override
            public void onReadyForSpeech(Bundle bundle) {
            }

            @Override
            public void onBeginningOfSpeech() {
                getEventEmitter().emit(ON_SPEECH_STARTED, null);
            }

            @Override
            public void onRmsChanged(float v) {
            }

            @Override
            public void onBufferReceived(byte[] bytes) {
            }

            @Override
            public void onEndOfSpeech() {
                getEventEmitter().emit(ON_SPEECH_STOPPED, null);
            }

            @Override
            public void onError(int i) {
                getEventEmitter().emit(ON_ERROR, i);
            }

            @Override
            public void onResults(Bundle bundle) {
                getEventEmitter().emit(ON_RESULTS, getFirstResult(bundle));
            }

            @Override
            public void onPartialResults(Bundle bundle) {
                getEventEmitter().emit(ON_PARTIAL_RESULTS, getFirstResult(bundle));
            }

            @Override
            public void onEvent(int i, Bundle bundle) {
            }
        });
    }

    private RCTDeviceEventEmitter getEventEmitter() {
        return reactContext.getJSModule(RCTDeviceEventEmitter.class);
    }

    @ReactMethod
    public void startListening(final String locale) {
        mainThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
                intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, locale);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, locale);
                speechRecognizer.startListening(intent);
            }
        });

    }

    @ReactMethod
    public void stopListening() {
        mainThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                speechRecognizer.stopListening();
            }
        });
    }

    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        constants.put(ON_SPEECH_STARTED, ON_SPEECH_STARTED);
        constants.put(ON_SPEECH_STOPPED, ON_SPEECH_STOPPED);
        constants.put(ON_PARTIAL_RESULTS, ON_PARTIAL_RESULTS);
        constants.put(ON_RESULTS, ON_RESULTS);
        constants.put(ON_ERROR, ON_ERROR);
        constants.put(TIMEOUT, SpeechRecognizer.ERROR_SPEECH_TIMEOUT);
        return constants;
    }

    @Override
    public String getName() {
        return "SpeechRecognition";
    }
}
