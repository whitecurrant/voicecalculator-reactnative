import React from 'react';
import { AppRegistry, View, Text } from 'react-native';

const App = () => (
  <View style={{ padding: 24, alignItems: 'center', justifyContent: 'center' }}>
    <Text>
      iOS currently not supported
    </Text>
  </View>
);
AppRegistry.registerComponent('VoiceCalculator', () => App);
