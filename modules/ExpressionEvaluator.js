const isOperator = x => /^[\*\/\+-]$/.test(x);
const isPriorityOperator = x => /^[\*\/]$/.test(x);
const isValidExpression = string =>
  /^(-?\d+\.?\d*(([x\+\-*\/]\d+\.?\d*)|((-{2})\d+\.?\d*)|((\+-)\d+\.?\d*))*)$/
    .test(string.replace(/\s([\*\/\+-])\s/g, '$1'));

/*
  Evaluate input string after validation. Split data into groups based on
  operators priority and reduce them into values in proper order.
*/
const evaluate = string =>
  new Promise((resolve, reject) => {
    const sanitazedString = sanitazeInput(string);
    if (!isValidExpression(sanitazedString)) {
      reject('Invalid expression.');
      return;
    }
    const operationQueque = buildOperationQueue(sanitazedString);
    const lowPriorityQueue = flattenOperationQueue(operationQueque);
    let result = reduceQueue(lowPriorityQueue);
    if (isNaN(result)) {
      reject('Result is not a number.');
    }
    if (!isFinite(result)) {
      result += ' 😱';
    }
    resolve({ result, prettyInput: sanitazedString });
  });

/*
  Ensure single spaces between operators (excluding minus sign for negative numbers)
  Replace phonetic operator with math symbols
*/
const sanitazeInput = string =>
  string
    .replace(/x/g, '*')
    .replace(/divided by/g, '/')
    .replace(/\//g, ' / ')
    .replace(/\*/g, ' * ')
    .replace(/\+/g, ' + ')
    .replace(/\s\s/g, ' ');

/*
  Flatten arrays containg high priority operations.
*/
const flattenOperationQueue = (operationQueque) => {
  const flattenedQueue = operationQueque.map((val) => {
    if (val instanceof Array) {
      return reduceQueue(val);
    }
    return val;
  });
  return flattenedQueue;
};

/*
  Reduce queue calculating new value based on previous operator and accumulated
  value.
*/
const reduceQueue = (queue) => {
  let lastOperator;
  const reducedQueue = queue.reduce((acc, val) => {
    if (isOperator(String(val))) {
      lastOperator = val;
      return acc;
    }
    const numValue = Number(val);
    switch (lastOperator) {
      case '+':
        return acc + numValue;
      case '-':
        return acc - numValue;
      case '*':
        return acc * numValue;
      case '/':
        return acc / numValue;
      default:
        return acc;
    }
  }, Number(queue[0]));
  return reducedQueue;
};

/*
  Parse valid input string checking whether following operators are privileged
  in evaluation order. This function performs grouping clusters of such operators
  with surrounding operands inside inner arrays. These arrays along with remaining
  values and low priority operator constitute operationQueue.
*/
const buildOperationQueue = (string) => {
  const operationQueue = [];
  const tokens = string.split(' ');
  // iterate over sequence peeking next operator
  for (let i = 0; i <= tokens.length - 2;) {
    const value = tokens[i];
    const operator = tokens[i + 1];
    // if high priority operator, create high priority queue
    if (isPriorityOperator(operator)) {
      const priorityQueque = [value, operator];
      // iterate over remaining sequence as long as high priority operators are chained
      let j = i + 2;
      for (; j < tokens.length - 1; j += 2) {
        const nextValue = tokens[j];
        const nextOperator = tokens[j + 1];
        if (isPriorityOperator(nextOperator)) {
          // push to priority queue next value and following operator
          priorityQueque.push(nextValue);
          priorityQueque.push(nextOperator);
        } else {
          // push remaining value to high priority queue
          // and push whole queue onto regular one with remaining operator
          priorityQueque.push(nextValue);
          operationQueue.push(priorityQueque);
          operationQueue.push(nextOperator);
          i = j + 2;
          break;
        }
      }
      // last value belongs to priority queque
      if (j === tokens.length - 1) {
        priorityQueque.push(tokens[j]);
        operationQueue.push(priorityQueque);
        break;
      }
      // last value belongs to regular queue
      if (i === tokens.length - 1) {
        operationQueue.push(tokens[i]);
      }
    } else {
      // low priority operator, push to regular queue
      operationQueue.push(value);
      operationQueue.push(operator);
      i += 2;
      // push dangling last value to regular queue
      if (i === tokens.length - 1) {
        operationQueue.push(tokens[i]);
      }
    }
  }
  return operationQueue;
};

export default {
  evaluate,
};
