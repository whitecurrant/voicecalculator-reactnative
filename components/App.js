import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  DeviceEventEmitter,
  PermissionsAndroid,
} from 'react-native';
import Tts from 'react-native-tts';
import SpeechRecognition from '../modules/SpeechRecognition';
import ExpressionEvaluator from '../modules/ExpressionEvaluator';
import MicButton from './MicButton';

const languageLocale = 'en';
Tts.setDefaultLanguage(languageLocale);

class App extends Component {
  constructor() {
    super();
    this.state = this.getInitialState();
  }

  getInitialState() {
    return {
      message: 'Press button to start',
      error: null,
      result: null,
      isListening: false,
      inputProvided: false,
    };
  }

  componentDidMount() {
    this.checkAndroidPermissions();
    DeviceEventEmitter.addListener(
      SpeechRecognition.ON_PARTIAL_RESULTS,
      this.onPartialResults.bind(this),
    );
    DeviceEventEmitter.addListener(
      SpeechRecognition.ON_SPEECH_STOPPED,
      this.onSpeechStopped.bind(this),
    );
    DeviceEventEmitter.addListener(
      SpeechRecognition.ON_ERROR,
      this.onSpeechError.bind(this),
    );
    DeviceEventEmitter.addListener(SpeechRecognition.ON_RESULTS, this.onResults.bind(this));
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeAllListeners();
  }

  async checkAndroidPermissions() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.RECORD_AUDIO, {
          title: 'Microphone Permission',
          message: 'This app needs access to your microphone',
        },
      );
      if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
        this.setState({
          error: 'This app won\'t without mic permission',
        });
      }
    } catch (err) {
      console.warn(err);
    }
  }

  onSpeechStopped() {
    this.setState({
      isListening: false,
    });
  }

  onPartialResults(result) {
    this.setState({
      message: result,
      inputProvided: true,
    });
  }

  onSpeechError(error) {
    if (error === SpeechRecognition.TIMEOUT) {
      this.setState(this.getInitialState());
      return;
    }
    this.setState({
      error: 'Internal speech recognizer error\nPlease try again.',
    });
  }

  onResults(voiceInput) {
    this.setState({
      message: voiceInput,
    });
    ExpressionEvaluator.evaluate(voiceInput)
      .then(({ result, prettyInput }) => {
        this.setState({
          result,
          message: prettyInput.concat(' = '),
        });
        Tts.speak(String(result));
      })
      .catch(() =>
        this.setState({
          error: 'Failed to recognize your expression.\nPlease try again.',
        }),
      );
  }

  startListening() {
    this.setState({
      isListening: true,
      error: null,
      result: null,
      message: 'Listening...',
    });
    SpeechRecognition.startListening(languageLocale);
  }

  renderAnswer() {
    return (
      <Text style={{ fontWeight: 'bold' }}>
        { this.state.result }
      </Text>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.titleStyle}>
          Voice Calculator
        </Text>
        <Text style={styles.messageStyle}>
          {this.state.message}
          {this.state.result && this.renderAnswer()}
        </Text>
        <MicButton
          style={styles.buttonStyle}
          visible={!this.state.isListening}
          onPress={() => this.startListening()}
        />
        <Text style={[styles.messageStyle, styles.errorStyle]}>
          {this.state.error}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleStyle: {
    position: 'absolute',
    top: 60,
    fontSize: 32,
  },
  buttonStyle: {
    margin: 20,
  },
  messageStyle: {
    textAlign: 'center',
    fontSize: 18,
    padding: 8,
  },
  errorStyle: {
    fontSize: 14,
    color: 'red',
  },
});

export default App;
