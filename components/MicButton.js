import React from 'react';
import {
  TouchableOpacity,
  Image,
  View,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';

const iconSource = require('../assets/microphone.png');

const MicButton = ({ visible, onPress, style }) => {
  if (!visible) return null;
  return (
    <View style={[styles.backgroundStyle, style]}>
      <TouchableOpacity onPress={onPress}>
        <Image source={iconSource} style={styles.iconStyle} />
      </TouchableOpacity>
    </View>
  );
};

MicButton.PropTypes = {
  visible: PropTypes.bool,
  onPress: PropTypes.func,
  style: View.propTypes.style,
};

const styles = StyleSheet.create({
  iconStyle: {
    width: 56,
    height: 56,
  },
  backgroundStyle: {
    width: 72,
    height: 72,
    padding: 8,
    elevation: 4,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#dfdfdf',
    borderRadius: 72,
  },
});

export default MicButton;
